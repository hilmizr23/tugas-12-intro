
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <! Agar data yang diinputkan tidak tampil di URL maka diberikan method="POST" >
    <form action="/welcome" method="POST">
        @csrf
        <label> First Name: </label> <br><br>
        <input type="text" name="first_name"> <br><br>

        <label> Last Name: </label> <br><br>
        <input type="text" name="last_name"> <br><br>

        <! Tipe radio hanya bisa memilih salah satu
           Dengan syarat diberi name >
        <label> Gender: </label> <br><br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other <br><br>

        <label> Nationality: </label> <br><br>
        <select name="nationality">
            <option value="1"> Indonesian </option>
            <option value="2"> Singaporean </option>
            <option value="3"> Thai </option>
            <option value="4"> Malaysian</option>
            <option value="5"> Japanese</option>
            <option value="6"> Chinese</option>
            <option value="7"> Russian</option>
            <option value="8"> German</option>
            <option value="9"> Vietnamese</option>
            <option value="10"> Indian</option>
            <option value="11"> Other</option>
        </select> <br><br>

        <! Tipe checkbox bisa memilih lebih dari satu >
        <label> Language Spoken: </label> <br><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other <br><br>

        <! Tipe textarea bisa diatur ukurannya dengan mengganti nilai cols dan rows>
        <label> Bio: </label> <br><br>
        <textarea name="alamat" cols="30" rows="10"></textarea> <br>

        <input type="submit" class="sign_up" value="Sign Up">
    </form>
</body>

</html>